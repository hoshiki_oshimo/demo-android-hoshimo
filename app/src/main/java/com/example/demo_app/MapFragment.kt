package com.example.demo_app

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng

class MapFragment : Fragment(), LocationListener {
    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val callback = OnMapReadyCallback { googleMap ->
            map = googleMap
            setUpMyLocation()
        }
        registerCallback(callback)
    }

    //region LocationListener
    override fun onLocationChanged(location: Location) {
        val latitude = location.latitude
        val longitude = location.longitude
        val coordinate = LatLng(latitude, longitude)
        val callback = OnMapReadyCallback { googleMap ->
            map = googleMap
            setUpLocationButton()
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 18.0f))
        }
        registerCallback(callback)
    }
    //endregion

    private fun setUpMyLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) return
        val locationManager = context?.getSystemService(LocationManager::class.java)
        locationManager?.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            1000,
            50f,
            this
        )
        map.isMyLocationEnabled = true
        setUpLocationButton()
    }

    private fun setUpLocationButton() {
        val locationButton =
            (view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                Integer.parseInt("2")
            ).layoutParams as RelativeLayout.LayoutParams
        locationButton.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        locationButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        locationButton.setMargins(0, 0, 30, 30)
    }

    private fun registerCallback(callback: OnMapReadyCallback) {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.container_view_google_map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }
}
