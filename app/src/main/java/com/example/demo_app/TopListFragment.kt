package com.example.demo_app

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_top_list.*

class TopListFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_top_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
        setUpPermission()
    }

    private fun setUpRecycler() {
        recycler_top_list.layoutManager = LinearLayoutManager(activity)
        recycler_top_list.adapter = TopListAdapter(::onItemClicked)
    }

    private fun onItemClicked(type: TopListItem) {
        when (type) {
            TopListItem.SLIDER -> {
                findNavController().navigate(R.id.action_topListFragment_to_pickerFragment)
            }
            TopListItem.MAP_VIEW -> {
                if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    findNavController().navigate(R.id.action_topListFragment_to_mapFragment)
                } else {
                    showGpsProviderDialog()
                }
            }
            else -> {
            }
        }
    }

    private fun setUpPermission() {
        if (!isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1000
            )
        }
    }

    private fun showGpsProviderDialog() {
        AlertDialog.Builder(requireContext())
            .setMessage(R.string.gps_provider_message)
            .setPositiveButton(R.string.button_dialog_setting) { _, _ ->
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            .show()
    }
}
