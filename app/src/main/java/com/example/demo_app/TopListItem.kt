package com.example.demo_app

enum class TopListItem(val title: String) {
    SLIDER("Slider"),
    DRAG_AND_DROP("Drag And Drop"),
    MAP_VIEW("Map View"),
    PLAY_VIDEO("Play Video"),
    WEB_VIEW("Web View"),
    VIEW_PAGER("View Pager"),
    GRID_VIEW("Grid View"),
    UNQUETE("Unquete"),
    SCROLL("Scroll")
}
