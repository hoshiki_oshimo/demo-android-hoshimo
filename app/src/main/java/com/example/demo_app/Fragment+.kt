package com.example.demo_app

import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

fun Fragment.isPermissionGranted(permission: String): Boolean {
    val result = ContextCompat.checkSelfPermission(requireContext(), permission)
    return result == PackageManager.PERMISSION_GRANTED
}
