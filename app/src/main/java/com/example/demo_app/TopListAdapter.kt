package com.example.demo_app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_text_top_list.view.*

class TopListAdapter(
    private val onItemClicked: (TopListItem) -> Unit
) : RecyclerView.Adapter<TopListAdapter.ViewHolder>() {
    private val topListItems: Array<TopListItem> = TopListItem.values()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_text_top_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.text_title.text = topListItems[position].title
        viewHolder.itemView.setOnClickListener { onItemClicked.invoke(topListItems[position]) }
    }

    override fun getItemCount() = topListItems.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
